"""
UI configuration template.
"""
from attrdict import AttrDict

import os


CFG = AttrDict(
    path=AttrDict(
        proj=os.path.dirname(os.path.dirname(__file__)),
        ui=os.path.dirname(__file__)
        ),
    ui=AttrDict(
        log_timestamps=True,
        ),
    )
CFG_FILE = f'{CFG.path.ui}/ui.cfg'


def save_config():
    """
    Save configuration.
    """
    print(f'[*] Saving configuration to {CFG_FILE}...')
    CFG.save(CFG_FILE)


def load_config():
    """
    Load configuration.
    """
    if os.path.isfile(CFG_FILE):
        print(f'[*] Loading configuration from {CFG_FILE}...')
        CFG.load(CFG_FILE)
