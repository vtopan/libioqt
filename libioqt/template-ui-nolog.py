#!/usr/bin/env python3
"""
...

Author: ...
"""
import os
import sys
import traceback

from PySide6.QtWidgets import QMainWindow, QApplication, QMessageBox, QWidget, \
        QStyleFactory
from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon, QKeySequence, QShortcut

from libioqt import create_layouts, VLabelEdit


__VER__ = '0.1.0.20220101'

APP_PATH = os.path.dirname(__file__)



class MainWindow(QMainWindow):

    def __init__(self, app):
        global MAINWINDOW
        super().__init__()
        MAINWINDOW = self
        self.setWindowTitle(...)
        self.clipboard = QApplication.clipboard()
        self.app = app
        self.setWindowIcon(QIcon(f'{APP_PATH}/....png'))
        self.create_ui_elements()
        self.setCentralWidget(self.main)
        QShortcut(QKeySequence("Esc"), self).activated.connect(self.quit)
        self.setFocusPolicy(Qt.StrongFocus)
        for k, v in self.wmap.items():
            if isinstance(v, QPushButton):
                method = 'clicked_' + k
                if hasattr(self, method):
                    self.wmap[k].clicked.connect(getattr(self, method))
                else:
                    sys.stderr.write(f'Missing implementation for {method}!\n')


    def create_ui_elements(self):
        """
        Create UI elements (widgets + layouts) and add them to self.wmap.
        """
        self.fieldmap = {}
        self.main = QWidget()
        widgets = [
            ...
        ]
        self.lay, self.wmap = create_layouts(widgets)
        self.main.setLayout(self.lay)


    def dbg(self, msg):
        """
        Print a debug message.
        """
        print(msg)


    def err(self, msg, title='ERROR'):
        """
        Pop an error.
        """
        QMessageBox(QMessageBox.Critical, title, msg, QMessageBox.Ok, self)


    def exc(self, msg):
        """
        Pop an exception (and exit).
        """
        self.dbg(traceback.format_exc())
        self.err(msg, 'CRASHED')


    def quit(self):
        """
        Close app.
        """
        self.app.quit()



def run(args=sys.argv):
    app = QApplication(args)
    app.setStyle(QStyleFactory.create("Plastique"))
    main = MainWindow(app)
    main.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    try:
        sys.exit(run(sys.argv))
    except Exception as e:
        sys.stderr.write(f'[!] Dead: {e}\n')
        raise
