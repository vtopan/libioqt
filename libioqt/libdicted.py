"""
Tree editor for a dict containing lists, dicts or basic values (int, str, etc.)

Can be used as a config editor / JSON editor / etc.

Todo:
- fix handling of Unicode characters

Author: vtopan/gmail
"""
from PySide6.QtWidgets import QFrame, QTreeView, QHBoxLayout, QPushButton, QTextEdit, \
    QLabel
from PySide6.QtGui import QStandardItemModel, QStandardItem, QKeySequence, QShortcut
from PySide6.QtCore import Qt


VER = '0.1.1.230928'


class VDictEditor(QFrame):
    """
    :param data: A dict containing dicts, lists and basic values.
    """

    def __init__(self, data):
        super().__init__()
        self.tree = QTreeView()
        self.tree.setAlternatingRowColors(True)
        self.model = QStandardItemModel()
        self.root = self.model.invisibleRootItem()
        self.tree.setModel(self.model)
        self.model.setHorizontalHeaderLabels(['Key', 'Value'])
        self.lay = QHBoxLayout()
        self.setLayout(self.lay)
        self.lay.addWidget(self.tree)
        self.load(data)


    def load(self, data):
        """
        Load a dict into the model.
        """
        self.data = data
        self.model.setRowCount(0)
        self._load(data, self.root)
        self.tree.expandAll()
        for i in range(self.model.columnCount()):
            self.tree.resizeColumnToContents(i)


    def _load(self, data, parent, indent=0):

        def _key_node(text, is_list=False):
            knode = QStandardItem(text)
            knode.setFlags(Qt.NoItemFlags)
            knode.is_list = is_list
            return knode

        for k, v in data.items():
            if isinstance(v, (list, dict)):
                if isinstance(v, dict):
                    parent.appendRow([_key_node(k)])
                    row = parent.child(parent.rowCount() - 1)
                    self._load(v, parent=row, indent=indent + 1)
                else:
                    parent.appendRow([_key_node(k, is_list=True)])
                    list_parent = parent.child(parent.rowCount() - 1)
                    for e in v:
                        list_parent.appendRow([_key_node('•')])
                        row = list_parent.child(list_parent.rowCount() - 1)
                        self._load(e, parent=row, indent=indent + 1)
            else:
                qi = QStandardItem(str(v) if v is not None else '')
                qi.name = k
                qi.type = type(v)
                parent.appendRow([_key_node(k), qi])


    def get_dict(self, root=None):
        """
        Convert the tree to a dict.
        """
        root = root or self.root
        is_list = getattr(root, 'is_list', False)
        res = [] if is_list else {}
        for i in range(root.rowCount()):
            knode = root.child(i, 0)
            key = knode.text()
            vnode = root.child(i, 1)
            if knode.rowCount():
                value = self.get_dict(knode)
            else:
                value, vtype = vnode.text(), vnode.type
                value = vtype(value) if vtype is not type(None) else None
            if is_list:
                res.append(value)
            else:
                res[key] = value
        return res



if __name__ == '__main__':
    from PySide6.QtWidgets import QApplication, QWidget
    from libioqt import create_layouts
    import json
    import sys

    app = QApplication([])
    window = QWidget()
    quit = lambda: app.quit()
    QShortcut(QKeySequence("Esc"), window).activated.connect(quit)
    filename = None
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        data = json.load(open(filename))
    else:
        data = {
            'one': {
                'two': 3,
                'three': [
                    {
                        'four': 'four',
                        'five': None
                    },
                    {
                        'six': 0.5
                    }
                ]
            }
        }
    tree = VDictEditor(data)
    lay, wmap = create_layouts([
        {'tree': tree},
        {'save': QPushButton('To JSON')},
        {'l1': QLabel('As JSON:'), 'json': QTextEdit()}
    ])
    wmap['save'].clicked.connect(lambda: wmap['json'].setText(json.dumps(tree.get_dict(), indent=2)))
    window.setLayout(lay)
    window.show()
    app.exec()
