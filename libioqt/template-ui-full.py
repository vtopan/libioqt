#!/usr/bin/env python3
"""
...

Author: ...
"""
from ui_config import CFG, load_config, save_config
from attrdict import AttrDict

from libioqt import create_layouts, VLabelEdit

from PySide6.QtWidgets import QMainWindow, QApplication, QMessageBox, QHBoxLayout, QVBoxLayout, \
        QPushButton, QWidget, QTextBrowser, QLabel, QLineEdit, QStatusBar, QTabWidget, \
        QFrame, QComboBox, QCompleter, QTableWidget, QTableWidgetItem, QDateEdit, QDateTimeEdit, \
        QCheckBox, QMessageBox, QTreeWidget, QTreeWidgetItem, QHeaderView, QGridLayout, QTextEdit, \
        QStyleFactory
from PySide6.QtCore import Qt, QCoreApplication, Signal, QThread, QByteArray, SIGNAL, Slot, \
        Signal, QDate, QDateTime, QEvent, QSortFilterProxyModel
from PySide6.QtGui import QClipboard, QFont, QIcon, QFontMetrics, QKeySequence, QDoubleValidator, \
        QDesktopServices, QColor, QPalette, QBrush, QPixmap, QTextCursor, QShortcut

import os
import functools
import sys
import threading
import time
import traceback




__VER__ = '0.1.0.20220101'

APP_PATH = os.path.dirname(__file__)
MAINWINDOW = None


def excepthook(exctype, value, traceback):
    sys.stderr.write(f'[!] {exctype}: {value}\n{traceback}\n')
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)
sys._excepthook = sys.excepthook
sys.excepthook = excepthook


def run_in_main(fun):
    @functools.wraps(fun)
    def _(*args, **kwargs):
        MAINWINDOW.rim(fun, args, kwargs)
    return _



class MainWindow(QMainWindow):

    set_ui_text_sig = Signal(str, str)
    run_in_main_sig = Signal(object, object, object)


    def __init__(self, app):
        global MAINWINDOW
        super().__init__()
        MAINWINDOW = self
        self.eventlog = None
        self.log_lock = threading.Lock()
        self.stop = 0
        self.setWindowTitle('...')
        self.clipboard = QApplication.clipboard()
        self.app = app
        self.fonts = {
            'fixed': QFont('Terminal', 8)
            }
        self.setWindowIcon(QIcon(f'{APP_PATH}/....png'))
        self.init_ui_elements()
        self.init_shortcuts()
        self.setCentralWidget(self.main)
        self.log('Hello, world! Press Esc to quit.')


    def init_shortcuts(self):
        """
        Initialize keyboard shortcuts.
        """
        QShortcut(QKeySequence("Esc"), self).activated.connect(self.quit)
        QShortcut(QKeySequence("Ctrl+S"), self).activated.connect(self.save_config)
        # QShortcut(QKeySequence("F5"), self).activated.connect(self.tab_changed)
        # for i in range(1, 10):
        #     QShortcut(QKeySequence(f"Alt+{i}"), self).activated.connect(functools.partial(self.change_tab, i - 1))


    def init_ui_elements(self):
        """
        Initialize UI elements (widgets + layouts) and add them to self.wmap.
        """
        self.setFocusPolicy(Qt.StrongFocus)
        self.run_in_main_sig.connect(self.runner_slot)
        self.fieldmap = {}
        self.main = QWidget()
        widgets = [
#            ...
            {'eventlog': QTextBrowser()},
        ]
        self.lay, self.wmap = create_layouts(widgets)
        self.wmap = AttrDict(self.wmap)
        self.main.setLayout(self.lay)
        # set up event log
        self.eventlog = self.wmap.eventlog
        self.eventlog.setStyleSheet('QScrollBar:vertical {width: 10px;}')
        self.eventlog.setPlaceholderText('Log messages')
        self.eventlog.setReadOnly(1)
        rowheight = QFontMetrics(self.eventlog.font()).lineSpacing()
        self.eventlog.setFixedHeight(10 + 4 * rowheight)
        self.eventlog.setOpenExternalLinks(True)
        self.eventlog.setFocus()
        # auto-connect buttons to clicked_* methods
        for k, v in self.wmap.items():
            if isinstance(v, QPushButton):
                method = 'clicked_' + k
                if hasattr(self, method):
                    self.wmap[k].clicked.connect(getattr(self, method))
                else:
                    sys.stderr.write(f'Missing implementation for {method}!\n')
        self.set_ui_text_sig.connect(self.set_ui_text_slot)
        

    def rim(self, fun, args=None, kwargs=None):
        """
        Run-in-main - run a function (or a QMainWindow method) in the main UI thread.
        """
        self.run_in_main_sig.emit(fun, args or [], kwargs or {})


    @Slot(object, object, object)
    def runner_slot(self, fun, args, kwargs):
        """
        Run-in-main slot.
        """
        if type(fun) is str:
            fun = getattr(self, fun)
        try:
            fun(*args, **kwargs)
        except Exception as e:
            self.exc(f'Crashed running {fun} in the main thread: {e}')


    def load_config(self):
        """
        Load & apply UI configuration.
        """
        if 'pos' in CFG.ui:
            self.move(*CFG.ui.pos)
        if 'size' in CFG.ui:
            self.resize(*CFG.ui.size)
        if CFG.ui.get('maximized', False):
            self.showMaximized()


    def save_config(self):
        """
        Save the configuration.
        """
        self._log('Saving configuration...')
        CFG.ui.pos = self.pos().toTuple()
        CFG.ui.size = self.size().toTuple()
        CFG.ui.maximized = self.isMaximized()
        save_config()


    @Slot(str, str)
    def set_ui_text_slot(self, node, value):
        """
        Slot for the .set_ui_text_sig() signal.
        """
        if node == 'eventlog':
            return self._log(value)
        raise ValueError(f'Unknown node {node}!')


    def _log(self, msg):
        """
        Local log - only to be called from the main UI thread.
        """
        print(f'[UI] {msg}')
        if not self.eventlog:
            return
        self.log_lock.acquire()
        if msg.startswith('[DEBUG]'):
            msg = f'<font color="#444">{msg}</font>'
        elif msg.startswith('[ERROR]') or msg.startswith('[CRASH]'):
            msg = f'<font color="#A00">{msg}</font>'
        if CFG.ui.log_timestamps:
            msg = f'<font color="#777">{time.strftime("[%d.%m.%y %H:%M:%S]")}</font> {msg}'
        self.eventlog.append(msg.replace('\n', '<br>'))
        self.eventlog.ensureCursorVisible()
        self.log_lock.release()


    def set_ui_text(self, node, value):
        """
        Allows setting UI elements from other threads (signal/slot mechanism).
        """
        value = str(value)
        self.set_ui_text_sig.emit(node, value)


    def log(self, msg):
        """
        Add message to the log.
        """
        self.set_ui_text('eventlog', msg)


    def dbg(self, msg):
        """
        Add a debug message to the log.
        """
        self.set_ui_text('eventlog', '[DEBUG] ' + msg)


    def err(self, msg):
        """
        Add an error message to the log.
        """
        self.set_ui_text('eventlog', '[ERROR] ' + msg)


    def exc(self, msg):
        """
        Add an error message to the log.
        """
        self.set_ui_text('eventlog', traceback.format_exc())
        self.set_ui_text('eventlog', f'[CRASH] {msg}')


    def quit(self):
        """
        Close app.
        """
        self.stop = 1
        self.save_config()
        self.app.quit()



def run(args=sys.argv):
    app = QApplication(args)
    app.setStyle(QStyleFactory.create("Plastique"))
    main = MainWindow(app)
    main.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    try:
        sys.exit(run(sys.argv))
    except Exception as e:
        sys.stderr.write(f'[!] Dead: {e}\n')
        raise
