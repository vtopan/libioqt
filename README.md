# libioqt

Pure-Python PySide6 (Qt6) components licensed under the terms of the MIT license.

Author: Vlad Ioan Topan


## libioqt.py

Provides the `create_layouts()` API which allows defining Qt UI components as Python data structures
and automatically creates layouts. From its manual:

    Converts recursive structures of lists, dicts and GUI widgets to a proper PySide2 layout.

    A list is converted to a QVBoxLayout, a dict to a QHBoxLayout, everything else is presumed
    to be a widget and simply added to the current layout. The keys in each dict are mapped to the
    corresponding widgets in the output `widget_map` dict.

    Known keywords:
    - '!tab' converts a dict to a QTabWidget
    - '!vertical' converts a dict to a QVBoxLayout instead of a QHBoxLayout
    - '!horizontal' converts a list to a QHBoxLayout instead of a QVBoxLayout
    - '!border' creates a border (QFrame) around the layout (the value can be CSS styling)
    - '!stretch' sets stretching rules (e.g. '!stretch': (0, 2) sets the stretch property of
        widget 0 to 2

    :return: (top_layout, widget_map)


## libqthexed.py

PySide2 Qt5 hex-viewer / hex-editor component (alpha stage, but functional).

